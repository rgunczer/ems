import Box from '@mui/material/Box';

import { PageHeader } from './components/PageHeader';
import { ParametersForm } from './components/ParametersForm';
import { Results } from './components/Results';


function App() {
  return (
    <>
      <PageHeader />
      <Box sx={{ display: 'flex', flexDirection: 'row', p: 1 }} >
        <ParametersForm />
        <Results />
      </Box>
    </>
  );
}

export default App;
