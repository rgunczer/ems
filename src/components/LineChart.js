import React from 'react';

// highcharts
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';


export const LineChart = ({ options }) => {
  return (
    <>
      <HighchartsReact
        highcharts={Highcharts}
        options={options}
      />
    </>
  );
};
