import React from 'react'

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


export const Indicators = () => {
    return (
      <Grid container spacing={2}>

        {/* row */}
        <Grid item xs={6}>
          <Item>Total RES production:</Item>
        </Grid>
        <Grid item xs={6}>
          <Item>Indicator:</Item>
        </Grid>

        {/* row */}
        <Grid item xs={6}>
          <Item>Indicator:</Item>
        </Grid>
        <Grid item xs={6}>
          <Item>Indicator:</Item>
        </Grid>

        {/* row */}
        <Grid item xs={6}>
          <Item>Indicator:</Item>
        </Grid>
        <Grid item xs={6}>
          <Item>Indicator:</Item>
        </Grid>

      </Grid>
    )
}
