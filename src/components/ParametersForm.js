import React from 'react';

import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';


export const ParametersForm = () => {
  const [scenario, setScenario] = React.useState('');
  const [consumption, setConsumption] = React.useState(100);
  const [resProduction, setResProduction] = React.useState(1000);

  const handleChangeScenario = event => {
    setScenario(event.target.value);
  };

  const handleChangeConsumption = event => {
    setConsumption(event.target.value);
  };

  const handleChangeRESProduction = event => {
    setResProduction(event.target.value);
  };

  return (
    <Container>

      <Typography variant="h5" gutterBottom component="div">
        Parameters
      </Typography>

      <FormControl variant="standard" fullWidth sx={{ mb: 2, minWidth: 300 }}>
        <InputLabel>Select scenario</InputLabel>
        <Select
          value={scenario}
          label="Select scenario"
          onChange={handleChangeScenario}
        >
          <MenuItem value={0}>Scenario 0</MenuItem>
          <MenuItem value={1}>Scenario 1</MenuItem>
          <MenuItem value={2}>Scenario 2</MenuItem>
        </Select>
      </FormControl>

      <FormControl variant="standard" fullWidth sx={{ mb: 2, minWidth: 300 }}>
        <TextField label="Consumption (kWh)" variant="standard" value={consumption} onChange={handleChangeConsumption} />
      </FormControl>

      <FormControl variant="standard" fullWidth sx={{ mb: 2, minWidth: 300 }}>
        <TextField label="RES Production (kWh)" variant="standard" value={resProduction} onChange={handleChangeRESProduction} />
      </FormControl>

      <Stack spacing={1} direction="row" justifyContent="flex-end">
        <Button variant="contained" onClick={ () => alert('reset') }>Reset</Button>
        <Button variant="contained" onClick={ () => alert('run') }>Run</Button>
      </Stack>

    </Container>
  );
};
