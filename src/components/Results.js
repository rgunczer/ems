import React from 'react';

// mui
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

// components
import { Indicators } from './Indicators';
import { LineChart } from './LineChart';

const chartOptions = [
  {
    title: {
      text: 'My chart Zero'
    },
    series: [{
      data: [1, 2, 3, 5, 4, 9, 0, 1]
    }]
  },
  {
    title: {
      text: 'My chart One'
    },
    series: [{
      data: [1, 2, 3, 5, 4, 9, 0, 1]
    }]
  },
  {
    title: {
      text: 'My chart Two'
    },
    series: [{
      data: [1, 2, 3, 5, 4, 9, 0, 1]
    }]
  }
];


export const Results = () => {


  return (
    <Container>

      <Typography variant="h5" gutterBottom component="div">
        Results
      </Typography>

      <Indicators />

      {chartOptions.map(chartOption => <LineChart options={chartOption} /> )}

    </Container>
  );
};
